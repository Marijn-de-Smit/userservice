package com.Winnable.UserService;

import com.Winnable.UserService.dto.LoginDTO;
import com.Winnable.UserService.dto.RegistrationDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import javax.persistence.EntityManagerFactory;
import javax.ws.rs.core.MediaType;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@WebAppConfiguration
@AutoConfigureMockMvc
@AutoConfigureTestEntityManager
public class AccountIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private EntityManagerFactory entityManagerFactory;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void register() throws Exception {
        mvc.perform( MockMvcRequestBuilders
            .post("/authentication/register/")
            .content(asJsonString(new RegistrationDTO("firstname", "lastname", "emailAddress", "phone", "password", "password")))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().is(403));
    }

    @Test
    public void login() throws Exception {
        mvc.perform( MockMvcRequestBuilders
            .post("/authentication/login/")
            .content(asJsonString(new LoginDTO("marijnds@gmail.com", "ww")))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().is(403));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
