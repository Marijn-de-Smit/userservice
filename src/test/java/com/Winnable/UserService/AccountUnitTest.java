package com.Winnable.UserService;

import com.Winnable.UserService.dto.RegistrationDTO;
import com.Winnable.UserService.repository.AccountRepository;
import com.Winnable.UserService.repository.VerificationTokenRepository;
import com.Winnable.UserService.service.AuthenticationService;
import com.Winnable.UserService.service.util.rabbitmq.RMQTaskSender;
import org.junit.Before;
import org.junit.Test;
import org.junit.platform.commons.logging.Logger;
import org.junit.platform.commons.logging.LoggerFactory;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class AccountUnitTest {

    private static final Logger logger = LoggerFactory.getLogger(AccountUnitTest.class);

    @InjectMocks
    private AuthenticationService authenticationService;

    @Mock
    AccountRepository accountRepository;

    @Mock
    VerificationTokenRepository verificationTokenRepository;

    @Mock
    RMQTaskSender messageSender;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void create(){
        RegistrationDTO dto = new RegistrationDTO("firstname", "lastname", "emailAddress", "phone", "password", "password");
        boolean result = authenticationService.register(dto);
        assertTrue(result);
    }

    @Test
    public void read(){
    }

    @Test
    public void update(){
    }

    @Test
    public void delete(){
    }
}
