package com.Winnable.UserService;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.util.concurrent.TimeUnit;

public class SeleniumAccountCreation {

    WebDriver browser;
    WebDriverWait wait;

    @Before
    public void SetUp(){
        System.setProperty("webdriver.chrome.driver", "../other/chromedriver.exe");
        browser = new ChromeDriver();
        wait = new WebDriverWait(browser, 5);
    }

    @Test
    public void testAccountCreation(){
        System.setProperty("webdriver.chrome.driver", "D:/Work/Fontys/S6/Individueel/winnable/other/chromedriver.exe");
        WebDriver browser = new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(browser, 5);

        browser.get("http://localhost:4200");
        browser.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        WebElement name = browser.findElement(By.id("firstname"));
        WebElement lastname = browser.findElement(By.id("lastname"));
        WebElement email = browser.findElement(By.id("email"));
        WebElement phone = browser.findElement(By.id("phone"));
        WebElement password = browser.findElement(By.id("password"));
        WebElement confirm = browser.findElement(By.id("passwordConfirm"));

        name.sendKeys("Marijn");
        lastname.sendKeys("de Smit");
        email.sendKeys("marijnds@gmail.com");
        phone.sendKeys("1234567890");
        password.sendKeys("test");
        confirm.sendKeys("test");

        WebElement registerButton = browser.findElement(By.id("register"));
        registerButton.click();
        browser.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }

    @After
    public void CloseDown(){
        browser.close();
        browser.quit();
        browser = null;
    }
}
