package com.Winnable.UserService.domain;

public enum Role {
    ADMINISTRATOR,
    USER,
    MANAGER,
    PROVIDER
}
