package com.Winnable.UserService.domain;

import com.Winnable.UserService.dto.TransactionDTO;
import javax.persistence.*;

@Entity
@Table(name = "wallet")
public class Wallet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "balance")
    private String balance;

    public Wallet() {
    }

    public Wallet(TransactionDTO dto)
    {
        this.balance = dto.getAfter();
    }

    public Wallet(String balance) {
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public String getBalance() {
        return balance;
    }
    public void setBalance(String balance) {
        this.balance = balance;
    }
}
