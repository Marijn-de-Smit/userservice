package com.Winnable.UserService.domain;

import com.Winnable.UserService.dto.accounts.AccountDTO;
import com.Winnable.UserService.dto.RegistrationDTO;
import com.Winnable.UserService.dto.WalletDTO;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

@Entity
@Cacheable
@Table(name = "account")
@org.hibernate.annotations.Cache(usage= CacheConcurrencyStrategy.READ_WRITE)
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "firstname")
    private String firstname;
    @Column(name = "lastname")
    private String lastname;
    @Column(name = "emailAddress")
    private String emailAddress;
    @Column(name = "phoneNumber")
    private String phoneNumber;
    @Column(name = "password")
    private String password;
    @Column(name = "balance")
    private double balance;
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private Role role;
    @Column(name = "enabled")
    private boolean enabled;

    public Account() {
    }

    public Account (AccountDTO dto)
    {
        this.emailAddress = dto.getEmailAddress();
        this.password = dto.getPassword();
        this.firstname = dto.getFirstname();
        this.lastname = dto.getLastname();
        this.phoneNumber = dto.getPhoneNumber();
        this.balance = dto.getBalance();
    }

    public Account (RegistrationDTO dto)
    {
        this.emailAddress = dto.getEmailAddress();
        this.firstname = dto.getFirstname();
        this.lastname = dto.getLastname();
        this.phoneNumber = dto.getPhone();
        this.balance = 0;
    }

    public Account (WalletDTO dto)
    {
        this.id = dto.getId();
        this.balance = dto.getBalance();
    }

    public Account(String emailAddress, String password) {
        this.emailAddress = emailAddress;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public String getEmailAddress() {
        return emailAddress;
    }
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }
    public void setRole(Role role) {
        this.role = role;
    }

    public String getFirstname() {
        return firstname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public double getBalance() {
        return balance;
    }
    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isEnabled() {
        return enabled;
    }
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
