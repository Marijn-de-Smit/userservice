package com.Winnable.UserService;

import com.Winnable.UserService.service.util.jwt.JwtRequestFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableCaching
@Configuration
@EnableJpaRepositories(basePackages = "com.Winnable.UserService.repository")
@EntityScan("com/Winnable/UserService/domain")
@ComponentScan("com.Winnable.UserService.configuration")
@ComponentScan("com.Winnable.UserService.service.util.jwt")
@ComponentScan("com.Winnable.UserService.repository")
@ComponentScan("com.Winnable.UserService.controller")
@ComponentScan("com.Winnable.UserService.service")
@EnableDiscoveryClient
@SpringBootApplication
public class UserServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserServiceApplication.class, args);
    }

    @Bean
    public FilterRegistrationBean registration(JwtRequestFilter filter) {
        FilterRegistrationBean registration = new FilterRegistrationBean(filter);
        registration.setEnabled(false);
        return registration;
    }
}
