package com.Winnable.UserService.dto.accounts;

public class UserDTO {

    private String firstname;
    private String lastname;
    private String emailAdress;
    private String phonenumber;
    private String password;

    public UserDTO(String firstname, String lastname, String emailAdress, String password, String phonenumber) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.emailAdress = emailAdress;
        this.password = password;
        this.phonenumber = phonenumber;
    }

    public UserDTO() {
    }

    public String getFirstname() {
        return firstname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmailAdress() {
        return emailAdress;
    }
    public void setEmailAdress(String email) {
        this.emailAdress = email;
    }

    public String getPhonenumber() {
        return phonenumber;
    }
    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }
}
