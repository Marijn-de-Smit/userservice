package com.Winnable.UserService.dto.accounts;

import com.Winnable.UserService.domain.Account;
import com.Winnable.UserService.domain.Role;

public class AccountDTO {

    private int id;
    private String emailAddress;
    private String password;
    private String firstname;
    private String lastname;
    private String phoneNumber;
    private double balance;
    private String token;
    private Role role;

    // Empty constructor for JSON
    public AccountDTO() {
    }

    public AccountDTO(String emailAddress, String password, String firstname, String lastname, String phoneNumber, String token, Role role) {
        this.emailAddress = emailAddress;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.phoneNumber = phoneNumber;
        this.token = token;
        this.role = role;
    }

    // Constructor for testing purposes
    public AccountDTO(String emailAddress, String password) {
        this.emailAddress = emailAddress;
        this.password = password;
    }

    public AccountDTO(String emailAddress, String password, String token) {
        this.emailAddress = emailAddress;
        this.password = password;
        this.token = token;
    }

    public AccountDTO(Account account, String token) {
        this.emailAddress = account.getEmailAddress();
        this.password = account.getPassword();
        this.firstname = account.getFirstname();
        this.lastname = account.getLastname();
        this.phoneNumber = account.getPhoneNumber();
        this.balance = account.getBalance();
        this.role = account.getRole();
        this.token = token;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }
    public void setToken(String tfaToken) {
        this.token = tfaToken;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    public String getEmailAddress() {
        return emailAddress;
    }

    public String getFirstname() {
        return firstname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public double getBalance() {
        return balance;
    }
    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Role getRole() {return role;}
    public void setRole(Role role) {this.role = role;}
}

