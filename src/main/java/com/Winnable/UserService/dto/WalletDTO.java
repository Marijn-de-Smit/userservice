package com.Winnable.UserService.dto;

public class WalletDTO {
    private int id;
    private double balance;

    // Empty constructor for JSON
    public WalletDTO() {
    }

    public WalletDTO(double balance) {
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public double getBalance() {
        return balance;
    }
    public void setBalance(double balance) {
        this.balance = balance;
    }
}
