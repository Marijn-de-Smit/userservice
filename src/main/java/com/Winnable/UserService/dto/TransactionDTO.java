package com.Winnable.UserService.dto;

public class TransactionDTO {

    private String amount;
    private String before;
    private String after;

    public TransactionDTO(){

    }

    public TransactionDTO(String amount, String before, String after) {
        this.amount = amount;
        this.before = before;
        this.after = after;
    }

    public String getAmount() {
        return amount;
    }
    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBefore() {
        return before;
    }
    public void setBefore(String before) {
        this.before = before;
    }

    public String getAfter() {
        return after;
    }
    public void setAfter(String after) {
        this.after = after;
    }
}
