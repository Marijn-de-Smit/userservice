package com.Winnable.UserService.service;

import com.Winnable.UserService.domain.Account;
import com.Winnable.UserService.domain.Role;
import com.Winnable.UserService.domain.VerificationToken;
import com.Winnable.UserService.dto.LoginDTO;
import com.Winnable.UserService.dto.RegistrationDTO;
import com.Winnable.UserService.repository.VerificationTokenRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import com.Winnable.UserService.repository.AccountRepository;
import com.Winnable.UserService.service.util.authentication.Encryptor;
import com.Winnable.UserService.service.util.authentication.TfaTokenGenerator;
import com.Winnable.UserService.service.util.rabbitmq.RMQTaskSender;
import javax.ejb.Stateless;

@Stateless
@Service
@CacheConfig(cacheNames = "authenticationCache")
public class AuthenticationService implements IAuthenticationService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private VerificationTokenRepository verificationTokenRepository;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RMQTaskSender messageSender;

    @Override
    @Cacheable(cacheNames = "LoginCache")
    public Account login(LoginDTO dto) {
        Account result = null;
        try {
            Account account = accountRepository.findByEmailAddress(dto.getMail());
            if (account.getEmailAddress() != null) {
                if(account.isEnabled()){
                    result = account;
                }
            }
        } catch (Exception e) {
            logger.info(e.getMessage());
        }
        return result;
    }

    @Override
    public boolean logout() {
        //TODO: Make sure that the token send in the header is expired!!
        return true;
    }

    @Override
    public boolean register(RegistrationDTO dto) {
        boolean isRegistered = false;
        Account exists = accountRepository.findByEmailAddress(dto.getEmailAddress());
        if (exists == null) {
            try {
                Account account = new Account(dto);
                account.setPassword(Encryptor.encryptPassword(account.getPassword()));
                account.setRole(Role.USER);
                Account createdAccount = accountRepository.save(account);
                VerificationToken token = new VerificationToken(0, TfaTokenGenerator.generateTfaToken(), createdAccount);
                verificationTokenRepository.save(token);
                messageSender.sendTFAMessage(token.getToken(), account.getEmailAddress());
                isRegistered = true;
            } catch (Exception ex) {
                isRegistered = false;
            }
        }
        return isRegistered;
    }

    @Override
    public boolean registrationConfirm(String token) {
        boolean validated = false;
        VerificationToken foundToken  = verificationTokenRepository.findByToken(token);
        if (foundToken != null) {
            try {
                Account account = accountRepository.findById(foundToken.getAccount().getId());
                account.setEnabled(true);
                accountRepository.save(account);
                validated = true;
            } catch (Exception ex) {
                logger.error(ex.getMessage());
            }
        }
        return validated;
    }



}
