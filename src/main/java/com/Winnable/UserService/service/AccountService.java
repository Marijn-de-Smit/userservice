package com.Winnable.UserService.service;

import com.Winnable.UserService.domain.Account;
import com.Winnable.UserService.domain.Role;
import com.Winnable.UserService.domain.VerificationToken;
import com.Winnable.UserService.dto.RegistrationDTO;
import com.Winnable.UserService.repository.AccountRepository;
import com.Winnable.UserService.repository.VerificationTokenRepository;
import com.Winnable.UserService.service.util.authentication.Encryptor;
import com.Winnable.UserService.service.util.authentication.TfaTokenGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.ejb.Stateless;

@Stateless
@Service
public class AccountService implements IAccountService {

    @Autowired
    private VerificationTokenRepository verificationTokenRepository;

    @Autowired
    private AccountRepository accountRepository;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public boolean addManager(RegistrationDTO accountDTO) {
        boolean isRegistered = false;
        Account account = new Account(accountDTO);
        if (accountRepository.findByEmailAddress(account.getEmailAddress()) == null) {
            try {
                account.setPassword(Encryptor.encryptPassword(account.getPassword()));
                account.setRole(Role.MANAGER);
                account.setEnabled(true);
                accountRepository.save(account);
                VerificationToken token = new VerificationToken(0, TfaTokenGenerator.generateTfaToken(), account);
                verificationTokenRepository.save(token);
                isRegistered = true;
            } catch (Exception ex) {
                isRegistered = false;
            }
        }
        return isRegistered;
    }

    @Override
    public boolean addProvider(RegistrationDTO accountDTO) {
        boolean isRegistered = false;
        Account account = new Account(accountDTO);
        if (accountRepository.findByEmailAddress(account.getEmailAddress()) == null) {
            try {
                account.setPassword(Encryptor.encryptPassword(account.getPassword()));
                account.setRole(Role.PROVIDER);
                account.setEnabled(true);
                accountRepository.save(account);
                VerificationToken token = new VerificationToken(0, TfaTokenGenerator.generateTfaToken(), account);
                verificationTokenRepository.save(token);
                isRegistered = true;
            } catch (Exception ex) {
                isRegistered = false;
            }
        }
        return isRegistered;
    }

    @Override
    public boolean deleteAccount(int accountId) {
        boolean isDeleted = false;
        if (accountId!=0) {
            try {
                Account account = accountRepository.findById(accountId);
                VerificationToken token = verificationTokenRepository.findByAccount(account);
                verificationTokenRepository.delete(token);
                accountRepository.delete(account);
                isDeleted= true;
            } catch (Exception ex) {
                logger.error(ex.getMessage());
            }
        }
        return isDeleted;
    }
}
