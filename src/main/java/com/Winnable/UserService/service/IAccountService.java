package com.Winnable.UserService.service;

import com.Winnable.UserService.dto.RegistrationDTO;
import org.springframework.stereotype.Service;
import javax.ejb.Local;

@Local
@Service
public interface IAccountService {

    boolean addManager(RegistrationDTO dto);
    boolean addProvider(RegistrationDTO dto);
    boolean deleteAccount(int accountId);
}
