package com.Winnable.UserService.service;

import com.Winnable.UserService.domain.Account;
import com.Winnable.UserService.dto.WalletDTO;
import com.Winnable.UserService.repository.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.ejb.Stateless;

@Stateless
@Service
public class WalletService implements IWalletService{

    @Autowired
    private AccountRepository accountRepository;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public boolean updateBalance(WalletDTO walletDTO) {
        boolean result = false;
        Account account = new Account(walletDTO);
        if (accountRepository.findByEmailAddress(account.getEmailAddress()) == null) {
            try {
                accountRepository.save(account);
                result = true;
            } catch (Exception ex) {
                logger.error(ex.getMessage());
            }
        }

        return result;
    }
}
