package com.Winnable.UserService.service;

import com.Winnable.UserService.domain.Account;
import com.Winnable.UserService.dto.LoginDTO;
import com.Winnable.UserService.dto.RegistrationDTO;
import org.springframework.stereotype.Service;
import javax.ejb.Local;

@Local
@Service
public interface IAuthenticationService {

    boolean register(RegistrationDTO dto);
    boolean registrationConfirm(String token);
    Account login(LoginDTO dto);
    boolean logout();
}
