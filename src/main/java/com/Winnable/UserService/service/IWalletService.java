package com.Winnable.UserService.service;

import com.Winnable.UserService.dto.WalletDTO;
import org.springframework.stereotype.Service;
import javax.ejb.Local;

@Local
@Service
public interface IWalletService {

    boolean updateBalance(WalletDTO walletDTO);
}
