package com.Winnable.UserService.service.util.authentication;

import org.mindrot.jbcrypt.BCrypt;

public abstract class Encryptor {

    public static String encryptPassword(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt());
    }

    public static boolean validatePassword(String originalPassword, String storedPassword) {
        return BCrypt.checkpw(originalPassword, storedPassword);
    }

}
