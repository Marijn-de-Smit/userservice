package com.Winnable.UserService.service.util.authentication;

import com.Winnable.UserService.dto.RegistrationDTO;
import org.springframework.stereotype.Component;

@Component
public class AccountAuthenticator {

    public boolean authenticate(RegistrationDTO account) {
        if (account != null) {
            if (isEmailEmpty(account.getEmailAddress())) {
                return false;
            }

            if (isPasswordEmpty(account.getPassword())) {
                return false;
            }

            return true;
        } else {
            return false;
        }
    }

    public boolean isEmailEmpty(String emailAddress) {
        return emailAddress == null;
    }

    public boolean isPasswordEmpty(String password) {
        return password == null;
    }

}
