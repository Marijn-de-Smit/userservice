package com.Winnable.UserService.service.util.rabbitmq;

import com.Winnable.UserService.dto.TfaDTO;
import com.google.gson.Gson;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Service
public class RMQTaskSender {

    @Autowired
    private RabbitTemplate rabbitTemplate;
    private CountDownLatch countDownLatch = new CountDownLatch(1);
    private final String userExchangeName = "mailserver-exchange";
    private Gson gson = new Gson();

    public void sendTFAMessage(String token, String mailAddress) {
        TfaDTO dto = new TfaDTO(mailAddress, token);
        rabbitTemplate.convertAndSend(userExchangeName, "pom", gson.toJson(dto));
        try {
            countDownLatch.await(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
