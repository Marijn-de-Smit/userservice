package com.Winnable.UserService.service.util.authentication;

import org.springframework.security.crypto.keygen.KeyGenerators;

public abstract class TfaTokenGenerator {

    public static String generateTfaToken() {
        return KeyGenerators.string().generateKey();
    }

}
