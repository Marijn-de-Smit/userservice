package com.Winnable.UserService.service.util.authentication;

import com.Winnable.UserService.service.util.jwt.JwtAuthentication;

public abstract class RoleAuthentication {

    public static boolean validateRole(String[] expectedRoles, JwtAuthentication jwtAuthentication) {
        boolean hasRole = false;

        for (String role : expectedRoles) {
            if (role.equals(jwtAuthentication.getRole())) {
                hasRole = true;
            }
        }

        return hasRole;
    }

}
