package com.Winnable.UserService.controller;

import com.Winnable.UserService.dto.WalletDTO;
import javax.ejb.Local;
import javax.ws.rs.core.Response;

@Local
public interface IWalletController {

    Response updateWallet(WalletDTO walletDTO);
}
