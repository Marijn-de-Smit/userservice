package com.Winnable.UserService.controller;

import com.Winnable.UserService.dto.RegistrationDTO;
import com.Winnable.UserService.service.util.authentication.AccountAuthenticator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.Winnable.UserService.service.AccountService;
import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;

@Stateless
@RestController
@RequestMapping("/user")
public class AccountController implements IAccountController {

    @Autowired
    private AccountService service;

    @Autowired
    private AccountAuthenticator authenticator;

    @PreAuthorize("hasRole('MANAGER')")
    @Override
    @PostMapping(path = "/addManager", consumes = "application/json", produces = "application/json")
    public Response addManager(@RequestBody RegistrationDTO dto) {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        if (authenticator.authenticate(dto)) {
            if (service.addManager(dto)) {
                response.status(Response.Status.CREATED);
            } else {
                response.status(Response.Status.SERVICE_UNAVAILABLE);
            }
        } else {
            response.status(Response.Status.CONFLICT);
        }
        return response.build();
    }

    @PreAuthorize("hasRole('MANAGER')")
    @Override
    @PostMapping(path = "/addProvider", consumes = "application/json", produces = "application/json")
    public Response addProvider(@RequestBody RegistrationDTO dto) {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        if (authenticator.authenticate(dto)) {
            if (service.addProvider(dto)) {
                response.status(Response.Status.CREATED);
            } else {
                response.status(Response.Status.SERVICE_UNAVAILABLE);
            }
        } else {
            response.status(Response.Status.CONFLICT);
        }
        return response.build();
    }

    @PreAuthorize("hasRole('MANAGER')")
    @Override
    @PostMapping(path = "/delete", consumes = "application/json", produces = "application/json")
    public Response deleteAccount(@NotNull @RequestBody int id) {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        if (service.deleteAccount(id)) {
            response.status(Response.Status.OK);
        }
        return response.build();
    }
}
