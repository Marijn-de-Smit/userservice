package com.Winnable.UserService.controller;

import com.Winnable.UserService.dto.RegistrationDTO;
import javax.ejb.Local;
import javax.ws.rs.core.Response;

@Local
public interface IAccountController {

    Response addManager(RegistrationDTO accountDTO);
    Response addProvider(RegistrationDTO accountDTO);
    Response deleteAccount(int accountId);
}
