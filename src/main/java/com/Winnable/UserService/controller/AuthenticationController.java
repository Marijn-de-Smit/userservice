package com.Winnable.UserService.controller;

import com.Winnable.UserService.domain.Account;
import com.Winnable.UserService.dto.accounts.AccountDTO;
import com.Winnable.UserService.dto.LoginDTO;
import com.Winnable.UserService.dto.RegistrationDTO;
import com.Winnable.UserService.service.AuthenticationService;
import com.Winnable.UserService.service.util.authentication.AccountAuthenticator;
import com.Winnable.UserService.service.util.jwt.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;

@Stateless
@RestController
@RequestMapping(path = "/authentication")
public class AuthenticationController implements IAuthenticationController {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private AccountAuthenticator authenticator;

    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @Override
    public Response login(@RequestBody LoginDTO dto) {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        Account account = authenticationService.login(dto);

        if (account != null) {
            String token = jwtTokenUtil.generateToken(account);
            AccountDTO accountDTO = new AccountDTO(account, token);
            accountDTO.setPassword(null);
            response.status(Response.Status.OK).entity(accountDTO);
        } else {
            response.status(Response.Status.UNAUTHORIZED).entity("User does not exist");
        }
        return response.build();
    }


    @PostMapping(value = "/logout", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @Override
    public Response logout() {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        boolean allowed = authenticationService.logout();

        if (allowed) {
            response.status(Response.Status.OK).entity("Until next time");
        } else {
            response.status(Response.Status.BAD_REQUEST).entity("Action is currently impossible");
        }
        return response.build();
    }


    @PostMapping(path = "/register", consumes = "application/json", produces = "application/json")
    public Response register(@RequestBody RegistrationDTO dto) {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        if (authenticator.authenticate(dto)) {
            if (authenticationService.register(dto)) {
                response.status(Response.Status.CREATED);
            } else {
                response.status(Response.Status.SERVICE_UNAVAILABLE);
            }
        } else {
            response.status(Response.Status.CONFLICT);
        }
        return response.build();
    }

    @GetMapping(path = "/registrationConfirm/{token}", produces = "application/json")
    @ResponseBody
    public Response registrationConfirm(@PathVariable String token) {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        if (token != null) {
            response = Response.status(Response.Status.SERVICE_UNAVAILABLE);
            if (authenticationService.registrationConfirm(token)) {
                response = Response.status(Response.Status.OK);
            }
        }
        return response.build();
    }
}
