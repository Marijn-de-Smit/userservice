package com.Winnable.UserService.controller;

import com.Winnable.UserService.dto.WalletDTO;
import com.Winnable.UserService.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;

@Stateless
@RestController
@RequestMapping("/wallet")
public class WalletController implements IWalletController {

    @Autowired
    private WalletService service;

    @Override
    @PostMapping(path = "/updateBalance", consumes = "application/json", produces = "application/json")
    public Response updateWallet(@NotNull @RequestBody WalletDTO walletDTO) {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        if (service.updateBalance(walletDTO)) {
            response.status(Response.Status.OK);
        }
        return response.build();
    }
}
