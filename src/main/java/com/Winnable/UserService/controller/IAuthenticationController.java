package com.Winnable.UserService.controller;

import com.Winnable.UserService.dto.LoginDTO;
import com.Winnable.UserService.dto.RegistrationDTO;
import javax.ejb.Local;
import javax.ws.rs.core.Response;

@Local
public interface IAuthenticationController {

    Response login(LoginDTO dto);
    Response logout();
    Response register(RegistrationDTO dto);
}
