package com.Winnable.UserService.repository;

import com.Winnable.UserService.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;
import javax.persistence.QueryHint;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {

    @QueryHints(@QueryHint(name= org.hibernate.annotations.QueryHints.CACHEABLE, value="true"))
    Account findByEmailAddress(String mail);
    Account findById(int id);
}
