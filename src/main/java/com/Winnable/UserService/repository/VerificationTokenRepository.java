package com.Winnable.UserService.repository;

import com.Winnable.UserService.domain.Account;
import com.Winnable.UserService.domain.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Integer> {

    VerificationToken findByToken(String token);
    VerificationToken findByAccount(Account account);

}